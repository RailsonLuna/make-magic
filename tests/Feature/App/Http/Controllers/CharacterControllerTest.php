<?php

namespace Tests\Feature\App\Http\Controllers;

use Doctrine\Tests\Common\Annotations\Fixtures\ClassDDC1660;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\CreatesApplication;
use Tests\TestCase;

/**
 * Class CharacterControllerTest
 * @package Tests\Feature\App\Http\Controllers
 */
class CharacterControllerTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase, CreatesApplication;

    const URL_CHARACTER = 'http://127.0.0.1:8000/api/character';

    public function testCreate()
    {
        $response = $this->post(self::URL_CHARACTER, [
            "name" => "Harry Potter",
            "role" => "student",
            "school" => "Hogwarts School of Witchcraft and Wizardry",
            "house" => "5a05e2b252f721a3cf2ea33f",
            "patronus" => "stag"
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        $data = json_decode($response->getContent());

        $response = $this->put(self::URL_CHARACTER . "/$data->id", [
            "name" => "Lord Voldemort",
            "role" => "student",
            "school" => "Hogwarts School of Witchcraft and Wizardry",
            "house" => "5a05e2b252f721a3cf2ea33f",
            "patronus" => "stag"
        ]);
        $response->assertStatus(Response::HTTP_OK);

        $response = $this->get(self::URL_CHARACTER . "/$data->id");
        $response->assertStatus(Response::HTTP_OK);

        $response = $this->delete(self::URL_CHARACTER . "/$data->id");
        $response->assertStatus(Response::HTTP_OK);

        $response = $this->get(self::URL_CHARACTER . "/$data->id");
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
