<?php

namespace App\Exceptions;

use DomainException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ServiceUnavailableException
 * @package App\Exceptions
 */
class HouseException extends DomainException
{
    /**
     * ServiceUnavailableException constructor.
     */
    public function __construct()
    {
        $this->message = 'House Invalid';
        $this->code = RESPONSE::HTTP_BAD_REQUEST;
            parent::__construct();
    }
}
