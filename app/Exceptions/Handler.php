<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Throwable $exception
     * @return JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception->getCode() >= 500) {
            return $this->internalServerError();
        }

        if ($exception instanceof NotFoundHttpException || $exception instanceof ModelNotFoundException) {
            return $this->pageNotFoundResponse();
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(
                ['message' => 'Method Not Allowed or Router Not Found'],
                Response::HTTP_METHOD_NOT_ALLOWED
            );
        }

        return $this->exceptionResponse($exception);
    }


    /**
     * @return JsonResponse
     */
    private function pageNotFoundResponse()
    {
        return response()->json(['message' => 'Not found'], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Throwable $exception
     * @return JsonResponse
     */
    private function exceptionResponse(Throwable $exception)
    {
        if ($exception->getCode() === 0) {
            return $this->internalServerError();
        }

        return response()->json(['message' => $exception->getMessage()], $exception->getCode());
    }

    /**
     * @return JsonResponse
     */
    private function internalServerError()
    {
        return response()->json(['message' => 'Internal Server Error'], 500);
    }
}
