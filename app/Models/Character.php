<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Character
 * @package App\Model
 */
class Character extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'role', 'school', 'house', 'patronus'];

    /**
     * @return array
     */
    public function getValidateRules(): array
    {
        return [
            'name' => 'required|string|min:11|max:255',
            'role' => 'required|string|min:11|max:255',
            'school' => 'required|string|min:11|max:255',
            'house' => 'required|string|min:11|max:255',
            'patronus' => 'required|string|min:11|max:255'
        ];
    }
}
