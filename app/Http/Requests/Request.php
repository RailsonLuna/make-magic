<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;

class Request extends FormRequest
{
    /**
     * @return mixed
     */
    public function data()
    {
        return cast_object($this->all());
    }

    /**
     * @return string
     */
    public function message()
    {
        return 'Os dados informados são inválidos.';
    }

    /**
     * @throws AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Unauthorized', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $response['message'] = $this->message();

        $response['errors'] = array_undot(
            $validator->errors()->toArray()
        );

        throw new HttpResponseException(
            response()->json($response ?? [], Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
