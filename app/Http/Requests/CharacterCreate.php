<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class CharacterCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            'role' => 'required|string|min:1|max:255',
            'school' => 'required|string|min:1|max:255',
            'house' => 'required|string|min:1|max:255',
            'patronus' => 'required|string|min:1|max:255'
        ];
    }

    /**
     * @return mixed
     */
    public function data()
    {
        return cast_object($this->all());
    }

    /**
     * @return string
     */
    public function message()
    {
        return 'Os dados informados são inválidos.';
    }

    /**
     * @throws AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException('Unauthorized', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $response['message'] = $this->message();

        $response['errors'] = array_undot(
            $validator->errors()->toArray()
        );

        throw new HttpResponseException(
            response()->json($response ?? [], Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
