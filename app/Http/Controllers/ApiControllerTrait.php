<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

trait ApiControllerTrait
{
    /**
     * ?limit=15
     * ?order=created_at,desc
     * ?where[id]=2
     * ?like[name]=name
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 20;
        $order = $data['order'] ?? null;
        $relationships = !isset($data['relationships']) ||
            $data['relationships'] === 'true' ? $this->relationships() : [];

        if ($order !== null) {
            $order = explode(',', $order);
        }

        $order[0] = $order[0] ?? 'id';
        $order[1] = $order[1] ?? 'asc';
        $where = $data['where'] ?? [];
        $like = null;

        if (!empty($data['like']) && is_array($data['like'])) {
            $like = $data['like'];
            $key = key(reset($like));
            $like[0] = $key;
            $like[1] = '%' . $like[$key] . '%';
        }

        $results = $this->model
            ->orderBy($order[0], $order[1])
            ->where(function ($query) use ($like) {
                if ($like) {
                    return $query->where($like[0], 'like', $like[1]);
                }
                return $query;
            })
            ->where($where)
            ->with($relationships)
            ->paginate($limit);


        return response()->json($results);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $result = $this->model
            ->with($this->relationships())
            ->findOrFail($id);

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $request->validate($this->rules ?? [], $this->messages ?? []);
        $result = $this->model->create($request->all());

        return response()->json($result, RESPONSE::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $result = $this->model->findOrFail($id);
        $result->update($request->all());

        return response()->json($result);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $result = $this->model->findOrFail($id);
        $result->delete();

        return response()->json($result);
    }

    /**
     * @return array
     */
    protected function relationships()
    {
        if (isset($this->relationships)) {
            return $this->relationships;
        }

        return [];
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function disable(int $id)
    {
        $result = $this->model->findOrFail($id);

        if (false !== $result->active) {
            $result->active = false;
            $result->save();
        }

        return response()->json($result, 200);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function activate(int $id)
    {
        $result = $this->model->findOrFail($id);

        if (true !== $result->active) {
            $result->active = true;
            $result->save();
        }

        return response()->json($result, 200);
    }
}
