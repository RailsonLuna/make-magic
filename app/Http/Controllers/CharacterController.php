<?php

namespace App\Http\Controllers;

use App\Http\Requests\CharacterCreate;
use App\Models\Character;
use App\Services\MagicMakeService;
use Illuminate\Http\JsonResponse;

class CharacterController
{
    use ApiControllerTrait;

    /**
     * @var Character
     */
    private $model;

    /**
     * @var MagicMakeService
     */
    private $magicMakeService;

    /**
     * CharacterController constructor.
     * @param Character $model
     * @param MagicMakeService $magicMakeService
     */
    public function __construct(Character $model, MagicMakeService $magicMakeService)
    {
        $this->model = $model;
        $this->magicMakeService = $magicMakeService;
    }

    /**
     * @param CharacterCreate $request
     * @return JsonResponse
     */
    public function post(CharacterCreate $request): JsonResponse
    {
        $this->magicMakeService->validateHouse($request->get('house'));
        return $this->create($request);
    }

    /**
     * @param CharacterCreate $request
     * @param int $id
     */
    public function put(CharacterCreate $request, int $id)
    {
        $this->magicMakeService->validateHouse($request->get('house'));
        $this->update($request, $id);
    }
}
