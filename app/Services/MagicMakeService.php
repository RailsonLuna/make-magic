<?php

namespace App\Services;

use App\Exceptions\HouseException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class MagicMakeService
{
    public function validateHouse(string $houseId)
    {
        $client = new Client();

        $response = $client->request(
            'GET',
            env('MAGIC_MALE_URL') . "houses/$houseId?key=" . env('MAGIC_KEY'),
            [
                'headers' => ['Content-Type' => 'application/json']
            ]
        );

        $content = json_decode($response->getBody()->getContents());

        if (isset($content->name) && $content->name === 'CastError') {
            throw new HouseException();
        }
    }
}
