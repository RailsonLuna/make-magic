<?php

if (!function_exists('cast_object')) {
    function cast_object($array)
    {
        return json_decode(json_encode($array));
    }
}

if (!function_exists('cast_array')) {
    function cast_array($data)
    {
        return (array) $data;
    }
}

if (!function_exists('array_undot')) {
    function array_undot(array $items, $delimiter = '.')
    {
        $new = [];

        foreach ($items as $key => $value) {
            if (strpos($key, $delimiter) === false) {
                $new[$key] = is_array($value) ? array_undot($value, $delimiter) : $value;
                continue;
            }

            $segments = explode($delimiter, $key);

            $last = &$new[$segments[0]];

            if (!is_null($last) && !is_array($last)) {
                throw new \LogicException(sprintf(
                    "The '%s' key has already been defined as being '%s'",
                    $segments[0],
                    gettype($last)
                ));
            }

            foreach ($segments as $k => $segment) {
                if ($k != 0) {
                    $last = &$last[$segment];
                }
            }

            $last = is_array($value) ? array_undot($value, $delimiter) : $value;
        }

        return $new;
    }
}
