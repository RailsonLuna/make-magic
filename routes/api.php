<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

$router->group(['prefix'=>'character'], function () use ($router) {
    $router->get('/', 'CharacterController@index');
    $router->post('/', 'CharacterController@post');
    $router->get('/{id}', 'CharacterController@show');
    $router->delete('/{id}', 'CharacterController@destroy');
    $router->put('/{id}', 'CharacterController@put');
});
