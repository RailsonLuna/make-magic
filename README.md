## Running Project

### Running by docker-composer
```bash
cp .env.example .env
sudo chmod 777 -R ../make-magic
docker-compose up
```
### Endpoints in this Project
 - GET http://localhost:8000/api/character
 - GET http://localhost:8000/api/character/{id}
 - POST http://localhost:8000/api/character
 - PUT http://localhost:8000/api/character/{id}
 - DELETE http://localhost:8000/api/character/{id}

### Run Tests
```bash
 docker exec -it make-magic-app php artisan test
```
### Code Qualitity
```bash
- docker exec -it make-magic-app  vendor/bin/phpmd app/ xml controversial
- docker exec -it make-magic-app  vendor/bin/phpmd app/ xml codesize
- docker exec -it make-magic-app  vendor/bin/phpcs app/ --standard=PSR2
- docker exec -it make-magic-app  vendor/bin/phpcs app/ --standard=PSR12
```
