---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_eb3824ba07b09e43e912c977a5c81f97 -->
## ?limit=15
?order=created_at,desc
?where[id]=2
?like[name]=name

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/character" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/character"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [],
    "first_page_url": "http:\/\/localhost\/api\/character?page=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/api\/character?page=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/api\/character",
    "per_page": 20,
    "prev_page_url": null,
    "to": null,
    "total": 0
}
```

### HTTP Request
`GET api/character`


<!-- END_eb3824ba07b09e43e912c977a5c81f97 -->

<!-- START_5a6caff7955ad48f4621c2db1eeb1468 -->
## api/character
> Example request:

```bash
curl -X POST \
    "http://localhost/api/character" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/character"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/character`


<!-- END_5a6caff7955ad48f4621c2db1eeb1468 -->

<!-- START_3654cf72bac403573e1e2fda24599ca4 -->
## api/character/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/character/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/character/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
{
    "message": "Not found"
}
```

### HTTP Request
`GET api/character/{id}`


<!-- END_3654cf72bac403573e1e2fda24599ca4 -->

<!-- START_1998b5ab97d7c8254b598d6f1fe5c0dd -->
## api/character/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/character/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/character/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/character/{id}`


<!-- END_1998b5ab97d7c8254b598d6f1fe5c0dd -->

<!-- START_cfdc8a64fa485939897200c1a2b6b956 -->
## api/character/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/character/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/character/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/character/{id}`


<!-- END_cfdc8a64fa485939897200c1a2b6b956 -->


